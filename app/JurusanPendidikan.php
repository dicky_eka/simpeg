<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JurusanPendidikan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jurusan_pendidikans';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['fc_kdjurpend', 'fv_nmjur', 'fc_kdpend', 'fc_kdgroupjur'];

    
}
