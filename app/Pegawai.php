<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
     protected $table = 't_pns';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['fc_nip', 'fv_nama', 'fc_nipbr', 'fc_glrdpn', 'fc_glrblk', 'fc_glrdpn', 'fc_glrblk', 'fc_kdtmplhr', 'fc_kdsex', 'fc_kdagama'];

    
}
