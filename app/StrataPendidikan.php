<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StrataPendidikan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_mstpend';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['fc_kdpend', 'fv_ket', 'NOMOR', 'fc_group', 'fc_kdgolru', 'fc_kdgolrumax'];

    
}
