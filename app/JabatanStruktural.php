<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JabatanStruktural extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tm_jabstruk';

    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
    


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['fc_kdjabstruk', 'fv_nmjabstruk', 'fc_singkatan', 'fv_jabatan'];

    
}
