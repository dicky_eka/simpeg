<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eselon extends Model
{
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_eselon';

    public $primaryKey  = 'fc_kdeselon';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['fc_kdeselon', 'fc_eselon', 'fm_tjStruk', 'fn_usiapensiun'];

    
}
