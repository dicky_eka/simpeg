<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GajiPokok extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_gaji';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['fc_periode', 'fc_kdgaji', 'fm_gaji'];

    
}
