<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiklatProfesi extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_diklatprof';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['fc_kdDiklat', 'fv_nmDiklat', 'fc_group'];

    
}
