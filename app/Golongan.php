<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Golongan extends Model
{
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_gol';

    public $primaryKey  = 'fc_kdgol';
  
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['fc_kdgol', 'fc_ruang', 'fv_nmjab'];

    
}
