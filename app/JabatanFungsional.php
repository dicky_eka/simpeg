<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JabatanFungsional extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_jabfung';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
    

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['fc_kdjabfung', 'fv_nmjabfung', 'fc_kdtenaga', 'fc_kdgolmax', 'fc_kdgolmin', 'fm_tunjangan', 'fn_bup'];

    
}
