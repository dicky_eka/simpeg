<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiklatStruktural extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_diklatstr';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['fc_kdDiklat', 'fv_nmDiklat'];

    
}
