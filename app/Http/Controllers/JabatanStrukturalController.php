<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\JabatanStruktural;
use Illuminate\Http\Request;

class JabatanStrukturalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $jabatanstruktural = JabatanStruktural::where('fc_kdjabstruk', 'LIKE', "%$keyword%")
                ->orWhere('fv_nmjabstruk', 'LIKE', "%$keyword%")
                ->orWhere('fc_singkatan', 'LIKE', "%$keyword%")
                ->orWhere('fv_jabatan', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $jabatanstruktural = JabatanStruktural::paginate($perPage);
        }

        return view('jabatan-struktural.index', compact('jabatanstruktural'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('jabatan-struktural.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'fc_kdjabstruk' => 'required:max:5',
            'fv_nmjabstruk' => 'required|max:20',
            'fc_singkatan' => 'max:50',
            'fv_jabatan' => 'max:50',
        ]);
        
        $requestData = $request->except(['_token','_method']);


        JabatanStruktural::create($requestData);

        return redirect('jabatan-struktural')->with('flash_message', 'JabatanStruktural added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $jabatanstruktural = JabatanStruktural::findOrFail($id);

        return view('jabatan-struktural.show', compact('jabatanstruktural'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $jabatanstruktural = JabatanStruktural::where('fc_kdjabstruk',$id)->first();

        return view('jabatan-struktural.edit', compact('jabatanstruktural'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'fc_kdjabstruk' => 'required:max:5',
            'fv_nmjabstruk' => 'required|max:20',
            'fc_singkatan' => 'max:50',
            'fv_jabatan' => 'max:50',
        ]);
        
        $requestData = $request->except(['_token','_method']);

        $jabatanstruktural = JabatanStruktural::where('fc_kdjabstruk',$id);

        $jabatanstruktural->update($requestData);

        return redirect('jabatan-struktural')->with('flash_message', 'JabatanStruktural updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        JabatanStruktural::where('fc_kdjabstruk',$id)->delete();

        return redirect('jabatan-struktural')->with('flash_message', 'JabatanStruktural deleted!');
    }
}
