<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kota;
use App\Kecamatan;

class WilayahController extends Controller
{
    public function getKota($provinsiID)
    {
    	return Kota::where('fc_kdprop',$provinsiID)->get();

    }

    public function getKecamatan($kotaID)
    {
    	return Kecamatan::where('fc_kdkota',$kotaID)->get();

    }
}
