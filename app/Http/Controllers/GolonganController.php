<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Golongan;
use Illuminate\Http\Request;

class GolonganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $golongan = Golongan::where('fc_kdgol', 'LIKE', "%$keyword%")
                ->orWhere('fc_ruang', 'LIKE', "%$keyword%")
                ->orWhere('fv_nmjab', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $golongan = Golongan::paginate($perPage);
        }

        return view('golongan.index', compact('golongan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('golongan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'fc_kdgol' => 'required',
            'fc_ruang' => 'required|max:5',
            'fv_nmjab' => 'required|max:50'
        ]);
        
        $requestData = $request->all();
        
        Golongan::create($requestData);

        return redirect('golongan')->with('flash_message', 'Golongan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $golongan = Golongan::findOrFail($id);

        return view('golongan.show', compact('golongan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $golongan = Golongan::where('fc_kdgol',$id)->first();

        return view('golongan.edit', compact('golongan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'fc_kdgol' => 'required',
            'fc_ruang' => 'required|max:5',
            'fv_nmjab' => 'required|max:50'
        ]);
        
        $requestData = $request->all();

        $golongan = Golongan::where('fc_kdgol',$id)->first();

        $golongan->update([
            'fc_kdgol' => $request->fc_kdgol,
            'fc_ruang' => $request->fc_ruang,
            'fv_nmjab' => $request->fv_nmjab
        ]);

        return redirect('golongan')->with('flash_message', 'Golongan updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Golongan::where('fc_kdgol',$id)->delete($id);

        return redirect('golongan')->with('flash_message', 'Golongan deleted!');
    }
}
