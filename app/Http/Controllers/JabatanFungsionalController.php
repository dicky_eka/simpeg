<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\JabatanFungsional;
use Illuminate\Http\Request;

class JabatanFungsionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $jabatanfungsional = JabatanFungsional::where('fc_kdjabfung', 'LIKE', "%$keyword%")
                ->orWhere('fv_nmjabfung', 'LIKE', "%$keyword%")
                ->orWhere('fc_kdtenaga', 'LIKE', "%$keyword%")
                ->orWhere('fc_kdgolmax', 'LIKE', "%$keyword%")
                ->orWhere('fc_kdgolmin', 'LIKE', "%$keyword%")
                ->orWhere('fm_tunjangan', 'LIKE', "%$keyword%")
                ->orWhere('fn_bup', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $jabatanfungsional = JabatanFungsional::paginate($perPage);
        }

        return view('jabatan-fungsional.index', compact('jabatanfungsional'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('jabatan-fungsional.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'fc_kdjabfung' => 'required|:max:5',
            'fv_nmjabfung' => 'required|max:50',
            'fc_kdtenaga' => 'max:2',
            'fc_kdgolmin' => 'max:2',
            'fc_kdgolmin' => 'max:2',
        ]);
        
        $requestData = $request->except(['_token','_method']);
        
        JabatanFungsional::create($requestData);

        return redirect('jabatan-fungsional')->with('flash_message', 'JabatanFungsional added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $jabatanfungsional = JabatanFungsional::findOrFail($id);

        return view('jabatan-fungsional.show', compact('jabatanfungsional'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $jabatanfungsional = JabatanFungsional::where('fc_kdjabfung',$id)->first();

        return view('jabatan-fungsional.edit', compact('jabatanfungsional'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'fc_kdjabfung' => 'required|:max:5',
            'fv_nmjabfung' => 'required|max:50',
            'fc_kdtenaga' => 'max:2',
            'fc_kdgolmin' => 'max:2',
            'fc_kdgolmin' => 'max:2',
        ]);
        
        $requestData = $request->except(['_token','_method']);
        
        $jabatanfungsional = JabatanFungsional::where('fc_kdjabfung',$id);
        $jabatanfungsional->update($requestData);

        return redirect('jabatan-fungsional')->with('flash_message', 'JabatanFungsional updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        JabatanFungsional::where('fc_kdjabfung',$id)->delete();

        return redirect('jabatan-fungsional')->with('flash_message', 'JabatanFungsional deleted!');
    }
}
