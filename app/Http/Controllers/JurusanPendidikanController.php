<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\JurusanPendidikan;
use Illuminate\Http\Request;

class JurusanPendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $jurusanpendidikan = JurusanPendidikan::where('fc_kdjurpend', 'LIKE', "%$keyword%")
                ->orWhere('fv_nmjur', 'LIKE', "%$keyword%")
                ->orWhere('fc_kdpend', 'LIKE', "%$keyword%")
                ->orWhere('fc_kdgroupjur', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $jurusanpendidikan = JurusanPendidikan::latest()->paginate($perPage);
        }

        return view('jurusan-pendidikan.index', compact('jurusanpendidikan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('jurusan-pendidikan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        JurusanPendidikan::create($requestData);

        return redirect('jurusan-pendidikan')->with('flash_message', 'JurusanPendidikan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $jurusanpendidikan = JurusanPendidikan::findOrFail($id);

        return view('jurusan-pendidikan.show', compact('jurusanpendidikan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $jurusanpendidikan = JurusanPendidikan::findOrFail($id);

        return view('jurusan-pendidikan.edit', compact('jurusanpendidikan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $jurusanpendidikan = JurusanPendidikan::findOrFail($id);
        $jurusanpendidikan->update($requestData);

        return redirect('jurusan-pendidikan')->with('flash_message', 'JurusanPendidikan updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        JurusanPendidikan::destroy($id);

        return redirect('jurusan-pendidikan')->with('flash_message', 'JurusanPendidikan deleted!');
    }
}
