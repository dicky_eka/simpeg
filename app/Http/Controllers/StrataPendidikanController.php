<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\StrataPendidikan;
use Illuminate\Http\Request;

class StrataPendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $stratapendidikan = StrataPendidikan::where('fc_kdpend', 'LIKE', "%$keyword%")
                ->orWhere('fv_ket', 'LIKE', "%$keyword%")
                ->orWhere('NOMOR', 'LIKE', "%$keyword%")
                ->orWhere('fc_group', 'LIKE', "%$keyword%")
                ->orWhere('fc_kdgolru', 'LIKE', "%$keyword%")
                ->orWhere('fc_kdgolrumax', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $stratapendidikan = StrataPendidikan::paginate($perPage);
        }

        return view('strata-pendidikan.index', compact('stratapendidikan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('strata-pendidikan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'fc_kdpend' => 'required:max:2',
            'fv_ket' => 'required:max:20',
            'NOMOR' => 'required|max:11',
            'fc_group' => 'required|max:1',
            'fc_kdgolru' => 'required|max:2',
        ]);
        
        $requestData = $request->except(['_token','_method']);
        
        StrataPendidikan::create($requestData);

        return redirect('strata-pendidikan')->with('flash_message', 'StrataPendidikan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $stratapendidikan = StrataPendidikan::findOrFail($id);

        return view('strata-pendidikan.show', compact('stratapendidikan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $stratapendidikan = StrataPendidikan::where('fc_kdpend',$id)->first();

        return view('strata-pendidikan.edit', compact('stratapendidikan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'fc_kdpend' => 'required:max:2',
            'fv_ket' => 'required:max:20',
            'fc_group' => 'required|max:1',
            'fc_kdgolru' => 'required|max:2',
        ]);
        
        $requestData = $request->except(['_token','_method']);
        
        $stratapendidikan = StrataPendidikan::where('fc_kdpend',$id);
        $stratapendidikan->update($requestData);

        return redirect('strata-pendidikan')->with('flash_message', 'StrataPendidikan updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        StrataPendidikan::destroy($id);

        return redirect('strata-pendidikan')->with('flash_message', 'StrataPendidikan deleted!');
    }
}
