<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\DiklatFungsional;
use Illuminate\Http\Request;

class DiklatFungsionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $diklatfungsional = DiklatFungsional::where('fc_kdDiklat', 'LIKE', "%$keyword%")
                ->orWhere('fv_nmDiklat', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $diklatfungsional = DiklatFungsional::latest()->paginate($perPage);
        }

        return view('diklat-fungsional.index', compact('diklatfungsional'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('diklat-fungsional.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        DiklatFungsional::create($requestData);

        return redirect('diklat-fungsional')->with('flash_message', 'DiklatFungsional added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $diklatfungsional = DiklatFungsional::findOrFail($id);

        return view('diklat-fungsional.show', compact('diklatfungsional'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $diklatfungsional = DiklatFungsional::findOrFail($id);

        return view('diklat-fungsional.edit', compact('diklatfungsional'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $diklatfungsional = DiklatFungsional::findOrFail($id);
        $diklatfungsional->update($requestData);

        return redirect('diklat-fungsional')->with('flash_message', 'DiklatFungsional updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DiklatFungsional::destroy($id);

        return redirect('diklat-fungsional')->with('flash_message', 'DiklatFungsional deleted!');
    }
}
