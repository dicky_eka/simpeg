<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Pegawai;
use App\Kota;
use App\Provinsi;
use App\StrataPendidikan;
use App\JusuranPendidikan;
use App\Agama;
use App\Golongan;
use App\JenisPegawai;
use App\Eselon;
use App\JabatanStruktural;
use App\JabatanFungsional;
use App\DiklatFungsional;
use App\DiklatStruktural;

use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $pegawai = Pegawai::where('fc_nip', 'LIKE', "%$keyword%")
                ->orWhere('fv_nama', 'LIKE', "%$keyword%")
                ->orWhere('fc_nipbr', 'LIKE', "%$keyword%")
                ->orWhere('fc_glrdpn', 'LIKE', "%$keyword%")
                ->orWhere('fc_glrblk', 'LIKE', "%$keyword%")
                ->orWhere('fc_glrdpn', 'LIKE', "%$keyword%")
                ->orWhere('fc_glrblk', 'LIKE', "%$keyword%")
                ->orWhere('fc_kdtmplhr', 'LIKE', "%$keyword%")
                ->orWhere('fc_kdsex', 'LIKE', "%$keyword%")
                ->orWhere('fc_kdagama', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $pegawai = Pegawai::paginate($perPage);
        }

        return view('pegawai.index', compact('pegawai'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $provinsi = Provinsi::pluck('fv_nmprop','fc_kdprop');
        $kota = Kota::pluck('fc_kdkota','fv_nmkota');
        $strataPendidikan = StrataPendidikan::pluck('fv_ket','fc_kdpend');
        $jurusanPendidikan = JusuranPendidikan::pluck('fv_nmjur','fc_kdjurpend');
        $sex = [1 => 'Laki-Laki',2=>'Perempuan'];
        $status = [1 => 'BELUM KAWIN',2=>'KAWIN',3=>'JANDA/DUDA'];
        $darah = [1 => 'A',2=>'B',3=>'AB',4=>'O'];
        $agama = Agama::pluck('fv_Agama','fc_kdAgama');
        $golongan = Golongan::pluck('fc_ruang','fc_kdgol');
        $statusPegawai = [1=>'PNS',2=>'CPNS',3=>'HONDA',4=>'P3K'];
        $jenisPegawai = JenisPegawai::pluck('fv_ket','fc_kdjnspeg');
        $eselon = Eselon::pluck('fc_eselon','fc_kdeselon');
        $pensiun = [1=>'PEGAWAI AKTIF',2=>'PENSIUN',3=>'MEINGGAL',4=>'MUTASI KELUAR',5=>'CLTN',6=>'PERMINTAAN SENDIRI',7=>'PEMBERHENTIAN DGN TIDAK HORMAT',8=>'PEMBERHENTIAN DGN HORMAT',9=>'PEMBERHENTIAN SEMENTARA',10=>'MPP',11=>'PENONAKTIFKAN'];

        $jenisJabatan = [1=>'FUNGSIONAL',2=>'STRUKTURAL'];

        $jabatanStruktural = JabatanStruktural::pluck('fv_nmjabstruk','fc_kdjabstruk');
        $jabatanFungsional = JabatanFungsional::pluck('fv_nmjabfung','fc_kdjabfung');
        $diklatStruktural = DiklatFungsional::pluck('fv_nmDiklat','fc_kdDiklat');
        $diklatFungsional = DiklatStruktural::pluck('fv_nmDiklat','fc_kdDiklat');

        
        return view('pegawai.create',compact('provinsi','kota','strataPendidikan','sex','status','darah','jurusanPendidikan','agama','golongan','statusPegawai','jenisPegawai','pensiun','eselon','jenisJabatan','jabatanStruktural','jabatanFungsional','diklatStruktural','diklatFungsional'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Pegawai::create($requestData);

        return redirect('pegawai')->with('flash_message', 'Pegawai added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $pegawai = Pegawai::findOrFail($id);

        return view('pegawai.show', compact('pegawai'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $pegawai = Pegawai::where('fc_nipbr',$id)->first();

        $provinsi = Provinsi::pluck('fv_nmprop','fc_kdprop');
        $kota = Kota::pluck('fc_kdkota','fv_nmkota');
        $strataPendidikan = StrataPendidikan::pluck('fv_ket','fc_kdpend');
        $jurusanPendidikan = JusuranPendidikan::pluck('fv_nmjur','fc_kdjurpend');
        $sex = [1 => 'Laki-Laki',2=>'Perempuan'];
        $status = [1 => 'BELUM KAWIN',2=>'KAWIN',3=>'JANDA/DUDA'];
        $darah = [1 => 'A',2=>'B',3=>'AB',4=>'O'];
        $agama = Agama::pluck('fv_Agama','fc_kdAgama');
        $golongan = Golongan::pluck('fc_ruang','fc_kdgol');
        $statusPegawai = [1=>'PNS',2=>'CPNS',3=>'HONDA',4=>'P3K'];
        $jenisPegawai = JenisPegawai::pluck('fv_ket','fc_kdjnspeg');
        $eselon = Eselon::pluck('fc_eselon','fc_kdeselon');
        $pensiun = [1=>'PEGAWAI AKTIF',2=>'PENSIUN',3=>'MEINGGAL',4=>'MUTASI KELUAR',5=>'CLTN',6=>'PERMINTAAN SENDIRI',7=>'PEMBERHENTIAN DGN TIDAK HORMAT',8=>'PEMBERHENTIAN DGN HORMAT',9=>'PEMBERHENTIAN SEMENTARA',10=>'MPP',11=>'PENONAKTIFKAN'];

        $jenisJabatan = [1=>'FUNGSIONAL',2=>'STRUKTURAL'];

        $jabatanStruktural = JabatanStruktural::pluck('fv_nmjabstruk','fc_kdjabstruk');
        $jabatanFungsional = JabatanFungsional::pluck('fv_nmjabfung','fc_kdjabfung');
        $diklatStruktural = DiklatFungsional::pluck('fv_nmDiklat','fc_kdDiklat');
        $diklatFungsional = DiklatStruktural::pluck('fv_nmDiklat','fc_kdDiklat');


        return view('pegawai.edit', compact('pegawai','provinsi','kota','strataPendidikan','sex','status','darah','jurusanPendidikan','agama','golongan','statusPegawai','jenisPegawai','pensiun','eselon','jenisJabatan','jabatanStruktural','jabatanFungsional','diklatStruktural','diklatFungsional'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $pegawai = Pegawai::findOrFail($id);
        $pegawai->update($requestData);

        return redirect('pegawai')->with('flash_message', 'Pegawai updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Pegawai::destroy($id);

        return redirect('pegawai')->with('flash_message', 'Pegawai deleted!');
    }
}
