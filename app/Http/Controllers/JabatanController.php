<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Jabatan;
use Illuminate\Http\Request;

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $jabatan = Jabatan::where('fc_kdjabatan', 'LIKE', "%$keyword%")
                ->orWhere('fv_jabatan', 'LIKE', "%$keyword%")
                ->orWhere('fc_kdsebut', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $jabatan = Jabatan::paginate($perPage);
        }

        return view('jabatan.index', compact('jabatan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('jabatan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'fc_kdjabatan' => 'required:max:5',
            'fv_jabatan' => 'required|max:50',
            'fc_kdsebut' => 'required|max:3',
        ]);
        
        $requestData = $request->all();
        
        Jabatan::create($requestData);

        return redirect('jabatan')->with('flash_message', 'Jabatan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $jabatan = Jabatan::findOrFail($id);

        return view('jabatan.show', compact('jabatan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $jabatan = Jabatan::where('fc_kdjabatan',$id)->first();

        return view('jabatan.edit', compact('jabatan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'fc_kdjabatan' => 'required:max:5',
            'fv_jabatan' => 'required|max:50',
            'fc_kdsebut' => 'required|max:3',
        ]);
        
        $requestData = $request->except(['_token','_method']);
        
        $jabatan = Jabatan::where('fc_kdjabatan',$id);
        $jabatan->update($requestData);

        return redirect('jabatan')->with('flash_message', 'Jabatan updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Jabatan::where('fc_kdjabatan',$id)->delete();
        

        return redirect('jabatan')->with('flash_message', 'Jabatan deleted!');
    }
}
