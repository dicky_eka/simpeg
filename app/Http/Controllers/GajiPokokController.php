<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\GajiPokok;
use Illuminate\Http\Request;

class GajiPokokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $gajipokok = GajiPokok::where('fc_periode', 'LIKE', "%$keyword%")
                ->orWhere('fc_kdgaji', 'LIKE', "%$keyword%")
                ->orWhere('fm_gaji', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $gajipokok = GajiPokok::paginate($perPage);
        }

        return view('gaji-pokok.index', compact('gajipokok'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('gaji-pokok.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'fc_periode' => 'required|:max:6',
            'fc_kdgaji' => 'required|max:4',
            'fm_gaji' => 'required',
        ]);
        
        $requestData = $request->except(['_token','_method']);
     
        GajiPokok::create($requestData);

        return redirect('gaji-pokok')->with('flash_message', 'GajiPokok added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $gajipokok = GajiPokok::findOrFail($id);

        return view('gaji-pokok.show', compact('gajipokok'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $gajipokok = GajiPokok::where('fc_kdgaji',$id)
                                ->where('fc_periode',request()->fc_periode)
                                ->first();


        return view('gaji-pokok.edit', compact('gajipokok'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'fm_gaji' => 'required',
        ]);

        $requestData = $request->except(['_token','_method']);
        
        $gajipokok = GajiPokok::where('fc_kdgaji',$id)
                                ->where('fc_periode',request()->fc_periode);

                                
        $gajipokok->update($requestData);

        return redirect('gaji-pokok')->with('flash_message', 'GajiPokok updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        GajiPokok::where('fc_kdgaji',$id)
                    ->where('fc_periode',request()->fc_periode)
                    ->delete();

        return redirect('gaji-pokok')->with('flash_message', 'GajiPokok deleted!');
    }
}
