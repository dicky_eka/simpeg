<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Struktural;
use Illuminate\Http\Request;

class StrukturalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $struktural = Struktural::where('fc_kdjabstruk', 'LIKE', "%$keyword%")
                ->orWhere('fv_nmjabstruk', 'LIKE', "%$keyword%")
                ->orWhere('fc_singkatan', 'LIKE', "%$keyword%")
                ->orWhere('fv_jabatan', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $struktural = Struktural::latest()->paginate($perPage);
        }

        return view('struktural.index', compact('struktural'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('struktural.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Struktural::create($requestData);

        return redirect('struktural')->with('flash_message', 'Struktural added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $struktural = Struktural::findOrFail($id);

        return view('struktural.show', compact('struktural'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $struktural = Struktural::findOrFail($id);

        return view('struktural.edit', compact('struktural'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $struktural = Struktural::findOrFail($id);
        $struktural->update($requestData);

        return redirect('struktural')->with('flash_message', 'Struktural updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Struktural::destroy($id);

        return redirect('struktural')->with('flash_message', 'Struktural deleted!');
    }
}
