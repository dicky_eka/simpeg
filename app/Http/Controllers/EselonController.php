<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Eselon;
use Illuminate\Http\Request;

class EselonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $eselon = Eselon::where('fc_kdeselon', 'LIKE', "%$keyword%")
                ->orWhere('fc_eselon', 'LIKE', "%$keyword%")
                ->orWhere('fm_tjStruk', 'LIKE', "%$keyword%")
                ->orWhere('fn_usiapensiun', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $eselon = Eselon::paginate($perPage);
        }

        return view('eselon.index', compact('eselon'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('eselon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'fc_kdeselon' => 'required',
            'fc_kdeselon' => 'required|max:6',
            'fm_tjStruk' => 'required',
            'fn_usiapensiun' => 'required'
        ]);

        $requestData = $request->all();
        
        Eselon::create($requestData);

        return redirect('eselon')->with('flash_message', 'Eselon added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $eselon = Eselon::findOrFail($id);

        return view('eselon.show', compact('eselon'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $eselon = Eselon::where('fc_kdeselon',$id)->first();

        return view('eselon.edit', compact('eselon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'fc_kdeselon' => 'required',
            'fc_kdeselon' => 'required|max:6',
            'fm_tjStruk' => 'required',
            'fn_usiapensiun' => 'required'
        ]);

        $requestData = $request->all();
        
        $eselon = Eselon::where('fc_kdeselon',$id);
        $eselon->update($requestData);

        return redirect('eselon')->with('flash_message', 'Eselon updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Eselon::where('fc_kdeselon',$id)->delete($id);

        return redirect('eselon')->with('flash_message', 'Eselon deleted!');
    }
}
