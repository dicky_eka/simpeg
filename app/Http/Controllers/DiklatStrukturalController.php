<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\DiklatStruktural;
use Illuminate\Http\Request;

class DiklatStrukturalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $diklatstruktural = DiklatStruktural::where('fc_kdDiklat', 'LIKE', "%$keyword%")
                ->orWhere('fv_nmDiklat', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $diklatstruktural = DiklatStruktural::latest()->paginate($perPage);
        }

        return view('diklat-struktural.index', compact('diklatstruktural'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('diklat-struktural.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        DiklatStruktural::create($requestData);

        return redirect('diklat-struktural')->with('flash_message', 'DiklatStruktural added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $diklatstruktural = DiklatStruktural::findOrFail($id);

        return view('diklat-struktural.show', compact('diklatstruktural'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $diklatstruktural = DiklatStruktural::findOrFail($id);

        return view('diklat-struktural.edit', compact('diklatstruktural'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $diklatstruktural = DiklatStruktural::findOrFail($id);
        $diklatstruktural->update($requestData);

        return redirect('diklat-struktural')->with('flash_message', 'DiklatStruktural updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DiklatStruktural::destroy($id);

        return redirect('diklat-struktural')->with('flash_message', 'DiklatStruktural deleted!');
    }
}
