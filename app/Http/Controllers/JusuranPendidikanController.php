<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\JusuranPendidikan;
use Illuminate\Http\Request;

class JusuranPendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $jusuranpendidikan = JusuranPendidikan::where('fc_kdjurpend', 'LIKE', "%$keyword%")
                ->orWhere('fv_nmjur', 'LIKE', "%$keyword%")
                ->orWhere('fc_kdpend', 'LIKE', "%$keyword%")
                ->orWhere('fc_kdgroupjur', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $jusuranpendidikan = JusuranPendidikan::paginate($perPage);
        }

        return view('jusuran-pendidikan.index', compact('jusuranpendidikan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('jusuran-pendidikan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'fc_kdjurpend' => 'required:max:7',
            'fv_nmjur' => 'required:max:80',
            'fc_kdpend' => 'required|max:2',
        ]);
        
        $requestData = $request->except(['_token','_method']);
        
        JusuranPendidikan::create($requestData);

        return redirect('jusuran-pendidikan')->with('flash_message', 'JusuranPendidikan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $jusuranpendidikan = JusuranPendidikan::where('fc_kdjurpend',$id)->first();

        return view('jusuran-pendidikan.show', compact('jusuranpendidikan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $jusuranpendidikan = JusuranPendidikan::where('fc_kdjurpend',$id)->first();

        return view('jusuran-pendidikan.edit', compact('jusuranpendidikan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'fc_kdjurpend' => 'required:max:7',
            'fv_nmjur' => 'required:max:80',
            'fc_kdpend' => 'required|max:2',
        ]);
        $requestData = $request->except(['_token','_method']);
        
        $jusuranpendidikan = JusuranPendidikan::where('fc_kdjurpend',$id);
        $jusuranpendidikan->update($requestData);

        return redirect('jusuran-pendidikan')->with('flash_message', 'JusuranPendidikan updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        JusuranPendidikan::where('fc_kdjurpend',$id)->destroy($id);

        return redirect('jusuran-pendidikan')->with('flash_message', 'JusuranPendidikan deleted!');
    }
}
