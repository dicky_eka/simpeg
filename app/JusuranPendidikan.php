<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JusuranPendidikan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_jurpend';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
    

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['fc_kdjurpend', 'fv_nmjur', 'fc_kdpend', 'fc_kdgroupjur'];

    
}
