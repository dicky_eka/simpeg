<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJabatanStrukturalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jabatan_strukturals', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('fc_kdjabstruk')->nullable();
            $table->string('fv_nmjabstruk')->nullable();
            $table->string('fc_singkatan')->nullable();
            $table->string('fv_jabatan')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jabatan_strukturals');
    }
}
