<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJabatanFungsionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jabatan_fungsionals', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('fc_kdjabfung')->nullable();
            $table->string('fv_nmjabfung')->nullable();
            $table->string('fc_kdtenaga')->nullable();
            $table->string('fc_kdgolmax')->nullable();
            $table->string('fc_kdgolmin')->nullable();
            $table->decimal('fm_tunjangan')->nullable();
            $table->integer('fn_bup')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jabatan_fungsionals');
    }
}
