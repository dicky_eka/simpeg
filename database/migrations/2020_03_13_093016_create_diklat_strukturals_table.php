<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDiklatStrukturalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diklat_strukturals', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('fc_kdDiklat')->nullable();
            $table->string('fv_nmDiklat')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('diklat_strukturals');
    }
}
