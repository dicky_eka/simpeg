<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePengawaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengawais', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('fc_nip')->nullable();
            $table->string('fv_nama')->nullable();
            $table->string('fc_nipbr')->nullable();
            $table->string('fc_glrdpn')->nullable();
            $table->string('fc_glrblk')->nullable();
            $table->string('fc_glrdpn')->nullable();
            $table->string('fc_glrblk')->nullable();
            $table->string('fc_kdtmplhr')->nullable();
            $table->string('fc_kdsex')->nullable();
            $table->string('fc_kdagama')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pengawais');
    }
}
