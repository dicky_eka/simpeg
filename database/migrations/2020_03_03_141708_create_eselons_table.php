<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEselonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eselons', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('fc_kdeselon')->nullable();
            $table->string('fc_eselon')->nullable();
            $table->decimal('fm_tjStruk')->nullable();
            $table->integer('fn_usiapensiun')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eselons');
    }
}
