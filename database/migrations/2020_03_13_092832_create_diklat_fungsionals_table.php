<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDiklatFungsionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diklat_fungsionals', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('fc_kdDiklat')->nullable();
            $table->string('fv_nmDiklat')->nullable();
            $table->string('nomor')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('diklat_fungsionals');
    }
}
