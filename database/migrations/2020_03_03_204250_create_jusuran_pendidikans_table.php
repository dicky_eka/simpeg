<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJusuranPendidikansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jusuran_pendidikans', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('fc_kdjurpend')->nullable();
            $table->string('fv_nmjur')->nullable();
            $table->string('fc_kdpend')->nullable();
            $table->string('fc_kdgroupjur')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jusuran_pendidikans');
    }
}
