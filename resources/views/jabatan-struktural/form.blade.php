<div class="form-group {{ $errors->has('fc_kdjabstruk') ? 'has-error' : ''}}">
    <label for="fc_kdjabstruk" class="control-label">{{ 'Fc Kdjabstruk' }}</label>
    <input class="form-control" name="fc_kdjabstruk" type="text" id="fc_kdjabstruk" value="{{ isset($jabatanstruktural->fc_kdjabstruk) ? $jabatanstruktural->fc_kdjabstruk : ''}}" >
    {!! $errors->first('fc_kdjabstruk', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fv_nmjabstruk') ? 'has-error' : ''}}">
    <label for="fv_nmjabstruk" class="control-label">{{ 'Fv Nmjabstruk' }}</label>
    <input class="form-control" name="fv_nmjabstruk" type="text" id="fv_nmjabstruk" value="{{ isset($jabatanstruktural->fv_nmjabstruk) ? $jabatanstruktural->fv_nmjabstruk : ''}}" >
    {!! $errors->first('fv_nmjabstruk', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fc_singkatan') ? 'has-error' : ''}}">
    <label for="fc_singkatan" class="control-label">{{ 'Fc Singkatan' }}</label>
    <input class="form-control" name="fc_singkatan" type="text" id="fc_singkatan" value="{{ isset($jabatanstruktural->fc_singkatan) ? $jabatanstruktural->fc_singkatan : ''}}" >
    {!! $errors->first('fc_singkatan', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fv_jabatan') ? 'has-error' : ''}}">
    <label for="fv_jabatan" class="control-label">{{ 'Fv Jabatan' }}</label>
    <input class="form-control" name="fv_jabatan" type="text" id="fv_jabatan" value="{{ isset($jabatanstruktural->fv_jabatan) ? $jabatanstruktural->fv_jabatan : ''}}" >
    {!! $errors->first('fv_jabatan', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
