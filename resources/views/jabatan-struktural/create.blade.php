@extends('layouts.app', ['activePage' => 'jabatan-struktural', 'titlePage' => __('Jabatan struktural')])


@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create New Jabatan Struktural</div>
                <div class="card-body">
                    <a href="{{ url('/jabatan-struktural') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />

                    @if ($errors->any())
                    <div class="alert alert-danger"  role="alert">
                        <ul >
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br>
                    @endif

                    <form method="POST" action="{{ url('/jabatan-struktural') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @include ('jabatan-struktural.form', ['formMode' => 'create'])

                    </form>

                </div>

            </div>
        </div>
    </div>
    </div>
</div>
@endsection
