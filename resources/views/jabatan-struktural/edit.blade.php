@extends('layouts.app', ['activePage' => 'jabatan-struktural', 'titlePage' => __('Jabatan struktural')])


@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
                    <div class="card-header">Edit JabatanStruktural #{{ $jabatanstruktural->fc_kdjabstruk }}</div>
                    <div class="card-body">
                        <a href="{{ url('/jabatan-struktural') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                       @if ($errors->any())
                        <div class="alert alert-danger"  role="alert">
                            <ul >
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <br>
                        @endif

                        <form action="{{ route('jabatan-struktural.update',$jabatanstruktural->fc_kdjabstruk) }}" method="POST">
                            @csrf
                            @method('PUT')

                            @include ('jabatan-struktural.form', ['formMode' => 'edit'])

                        </form>

                    </div>
                </div>
        </div>
    </div>
    </div>
</div>
@endsection
