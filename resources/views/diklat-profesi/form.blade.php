<div class="form-group {{ $errors->has('fc_kdDiklat') ? 'has-error' : ''}}">
    <label for="fc_kdDiklat" class="control-label">{{ 'Fc Kddiklat' }}</label>
    <input class="form-control" name="fc_kdDiklat" type="text" id="fc_kdDiklat" value="{{ isset($diklatprofesi->fc_kdDiklat) ? $diklatprofesi->fc_kdDiklat : ''}}" >
    {!! $errors->first('fc_kdDiklat', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fv_nmDiklat') ? 'has-error' : ''}}">
    <label for="fv_nmDiklat" class="control-label">{{ 'Fv Nmdiklat' }}</label>
    <input class="form-control" name="fv_nmDiklat" type="text" id="fv_nmDiklat" value="{{ isset($diklatprofesi->fv_nmDiklat) ? $diklatprofesi->fv_nmDiklat : ''}}" >
    {!! $errors->first('fv_nmDiklat', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fc_group') ? 'has-error' : ''}}">
    <label for="fc_group" class="control-label">{{ 'Fc Group' }}</label>
    <input class="form-control" name="fc_group" type="text" id="fc_group" value="{{ isset($diklatprofesi->fc_group) ? $diklatprofesi->fc_group : ''}}" >
    {!! $errors->first('fc_group', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
