@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">GajiPokok {{ $gajipokok->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/gaji-pokok') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/gaji-pokok/' . $gajipokok->id . '/edit') }}" title="Edit GajiPokok"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('gajipokok' . '/' . $gajipokok->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete GajiPokok" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $gajipokok->id }}</td>
                                    </tr>
                                    <tr><th> Fc Periode </th><td> {{ $gajipokok->fc_periode }} </td></tr><tr><th> Fc Kdgaji </th><td> {{ $gajipokok->fc_kdgaji }} </td></tr><tr><th> Fm Gaji </th><td> {{ $gajipokok->fm_gaji }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
