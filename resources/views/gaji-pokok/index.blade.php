@extends('layouts.app', ['activePage' => 'gaji-pokok', 'titlePage' => __('Gaji Pokok')])

@section('content')
  <div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Gaji Pokok</div>
                    <div class="card-body">
                        <a href="{{ url('/gaji-pokok/create') }}" class="btn btn-success btn-sm" title="Add New GajiPokok">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <form method="GET" action="{{ url('/gaji-pokok') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                       <th>Fc Periode</th><th>Fc Kdgaji</th><th>Fm Gaji</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($gajipokok as $item)
                                    <tr>
                                        <td>{{ $item->fc_periode }}</td><td>{{ $item->fc_kdgaji }}</td><td>{{ $item->fm_gaji }}</td>
                                        <td>
                                            <a href="{{ url('/gaji-pokok/' . $item->fc_kdgaji . '/edit'.'?fc_periode='. $item->fc_periode) }}" title="Edit GajiPokok"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/gaji-pokok' . '/' . $item->fc_kdgaji.'?fc_periode='. $item->fc_periode) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete GajiPokok" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $gajipokok->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


