<div class="form-group {{ $errors->has('fc_periode') ? 'has-error' : ''}}">
    <label for="fc_periode" class="control-label">{{ 'Fc Periode' }}</label>
    <input class="form-control" name="fc_periode" type="text" id="fc_periode" value="{{ isset($gajipokok->fc_periode) ? $gajipokok->fc_periode : ''}}"   disabled="{{ $formMode === 'edit' ? 'true' : '' }}">
    {!! $errors->first('fc_periode', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fc_kdgaji') ? 'has-error' : ''}}">
    <label for="fc_kdgaji" class="control-label">{{ 'Fc Kdgaji' }}</label>
    <input class="form-control" name="fc_kdgaji" type="text" id="fc_kdgaji" value="{{ isset($gajipokok->fc_kdgaji) ? $gajipokok->fc_kdgaji : ''}}"   disabled="{{ $formMode === 'edit' ? 'true' : '' }}" >
    {!! $errors->first('fc_kdgaji', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fm_gaji') ? 'has-error' : ''}}">
    <label for="fm_gaji" class="control-label">{{ 'Fm Gaji' }}</label>
    <input class="form-control" name="fm_gaji" type="text" id="fm_gaji" value="{{ isset($gajipokok->fm_gaji) ? $gajipokok->fm_gaji : ''}}" >
    {!! $errors->first('fm_gaji', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
