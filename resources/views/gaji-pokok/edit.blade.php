@extends('layouts.app', ['activePage' => 'gaji-pokok', 'titlePage' => __('Gaji Pokok')])


@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
                    <div class="card-header">Edit GajiPokok #{{ $gajipokok->fc_kdgaji }}</div>
                    <div class="card-body">
                        <a href="{{ url('/gaji-pokok') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                       @if ($errors->any())
                        <div class="alert alert-danger"  role="alert">
                            <ul >
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <br>
                        @endif

                        <form action="{{ route('gaji-pokok.update',$gajipokok->fc_kdgaji) }}" method="POST">
                            @csrf
                            @method('PUT')

                            @include ('gaji-pokok.form', ['formMode' => 'edit'])

                        </form>

                    </div>
                </div>
        </div>
    </div>
    </div>
</div>
@endsection