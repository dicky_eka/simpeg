<div class="form-group {{ $errors->has('fc_kdgol') ? 'has-error' : ''}}">
    <label for="fc_kdgol" class="control-label">{{ 'Fc Kdgol' }}</label>
    <input class="form-control" name="fc_kdgol" type="text" id="fc_kdgol" value="{{ isset($golongan->fc_kdgol) ? $golongan->fc_kdgol : ''}}" >
    {!! $errors->first('fc_kdgol', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fc_ruang') ? 'has-error' : ''}}">
    <label for="fc_ruang" class="control-label">{{ 'Fc Ruang' }}</label>
    <input class="form-control" name="fc_ruang" type="text" id="fc_ruang" value="{{ isset($golongan->fc_ruang) ? $golongan->fc_ruang : ''}}" >
    {!! $errors->first('fc_ruang', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fv_nmjab') ? 'has-error' : ''}}">
    <label for="fv_nmjab" class="control-label">{{ 'Fv Nmjab' }}</label>
    <input class="form-control" name="fv_nmjab" type="text" id="fv_nmjab" value="{{ isset($golongan->fv_nmjab) ? $golongan->fv_nmjab : ''}}" >
    {!! $errors->first('fv_nmjab', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
