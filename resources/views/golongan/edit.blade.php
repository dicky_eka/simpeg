@extends('layouts.app', ['activePage' => 'golongan', 'titlePage' => __('Golongan')])


@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit Golongan #{{ $golongan->id }}</div>
                <div class="card-body">
                    <a href="{{ url('/golongan') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />

                    @if ($errors->any())
                    <div class="alert alert-danger"  role="alert">
                        <ul >
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br>
                    <br>
                    @endif

                    <form action="{{ route('golongan.update',$golongan->fc_kdgol) }}" method="POST">
                            @csrf
                            @method('PUT')

                        @include ('golongan.form', ['formMode' => 'edit'])

                    </form>

                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
