@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Golongan {{ $golongan->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/golongan') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/golongan/' . $golongan->id . '/edit') }}" title="Edit Golongan"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('golongan' . '/' . $golongan->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Golongan" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $golongan->id }}</td>
                                    </tr>
                                    <tr><th> Fc Kdgol </th><td> {{ $golongan->fc_kdgol }} </td></tr><tr><th> Fc Ruang </th><td> {{ $golongan->fc_ruang }} </td></tr><tr><th> Fv Nmjab </th><td> {{ $golongan->fv_nmjab }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
