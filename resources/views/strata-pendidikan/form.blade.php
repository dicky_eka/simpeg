<div class="form-group {{ $errors->has('fc_kdpend') ? 'has-error' : ''}}">
    <label for="fc_kdpend" class="control-label">{{ 'Fc Kdpend' }}</label>
    <input class="form-control" name="fc_kdpend" type="text" id="fc_kdpend" value="{{ isset($stratapendidikan->fc_kdpend) ? $stratapendidikan->fc_kdpend : ''}}" >
    {!! $errors->first('fc_kdpend', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fv_ket') ? 'has-error' : ''}}">
    <label for="fv_ket" class="control-label">{{ 'Fv Ket' }}</label>
    <input class="form-control" name="fv_ket" type="text" id="fv_ket" value="{{ isset($stratapendidikan->fv_ket) ? $stratapendidikan->fv_ket : ''}}" >
    {!! $errors->first('fv_ket', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('NOMOR') ? 'has-error' : ''}}">
    <label for="NOMOR" class="control-label">{{ 'Nomor' }}</label>
    <input class="form-control" name="NOMOR" type="text" id="NOMOR" value="{{ isset($stratapendidikan->NOMOR) ? $stratapendidikan->NOMOR : ''}}" >
    {!! $errors->first('NOMOR', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fc_group') ? 'has-error' : ''}}">
    <label for="fc_group" class="control-label">{{ 'Fc Group' }}</label>
    <input class="form-control" name="fc_group" type="text" id="fc_group" value="{{ isset($stratapendidikan->fc_group) ? $stratapendidikan->fc_group : ''}}" >
    {!! $errors->first('fc_group', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fc_kdgolru') ? 'has-error' : ''}}">
    <label for="fc_kdgolru" class="control-label">{{ 'Fc Kdgolru' }}</label>
    <input class="form-control" name="fc_kdgolru" type="text" id="fc_kdgolru" value="{{ isset($stratapendidikan->fc_kdgolru) ? $stratapendidikan->fc_kdgolru : ''}}" >
    {!! $errors->first('fc_kdgolru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fc_kdgolrumax') ? 'has-error' : ''}}">
    <label for="fc_kdgolrumax" class="control-label">{{ 'Fc Kdgolrumax' }}</label>
    <input class="form-control" name="fc_kdgolrumax" type="text" id="fc_kdgolrumax" value="{{ isset($stratapendidikan->fc_kdgolrumax) ? $stratapendidikan->fc_kdgolrumax : ''}}" >
    {!! $errors->first('fc_kdgolrumax', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
