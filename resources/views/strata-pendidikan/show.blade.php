@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">StrataPendidikan {{ $stratapendidikan->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/strata-pendidikan') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/strata-pendidikan/' . $stratapendidikan->id . '/edit') }}" title="Edit StrataPendidikan"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('stratapendidikan' . '/' . $stratapendidikan->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete StrataPendidikan" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $stratapendidikan->id }}</td>
                                    </tr>
                                    <tr><th> Fc Kdpend </th><td> {{ $stratapendidikan->fc_kdpend }} </td></tr><tr><th> Fv Ket </th><td> {{ $stratapendidikan->fv_ket }} </td></tr><tr><th> NOMOR </th><td> {{ $stratapendidikan->NOMOR }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
