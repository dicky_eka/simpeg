@extends('layouts.app', ['activePage' => 'strata-pendidikan', 'titlePage' => __('Strata Pendidikan')])

@section('content')
  <div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Strata Pendidikan</div>
                    <div class="card-body">
                        <a href="{{ url('/strata-pendidikan/create') }}" class="btn btn-success btn-sm" title="Add New StrataPendidikan">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <form method="GET" action="{{ url('/strata-pendidikan') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Kode</th><th>Keterangan</th><th>Nomor Urut</th><th>Group</th><th>Gol/Ruang</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($stratapendidikan as $item)
                                    <tr>
                                        <td>{{ $item->fc_kdpend }}</td>
                                        <td>{{ $item->fv_ket }}</td>
                                        <td>{{ $item->NOMOR }}</td>
                                        <td>{{ $item->fc_group }}</td>
                                        <td>{{ $item->fc_kdgolru }}</td>
                                        <td>
                                            <a href="{{ url('/strata-pendidikan/' . $item->fc_kdpend . '/edit') }}" title="Edit StrataPendidikan"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/strata-pendidikan' . '/' . $item->fc_kdpend) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete StrataPendidikan" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $stratapendidikan->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
