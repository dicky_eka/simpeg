@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Struktural {{ $struktural->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/struktural') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/struktural/' . $struktural->id . '/edit') }}" title="Edit Struktural"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('struktural' . '/' . $struktural->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Struktural" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $struktural->id }}</td>
                                    </tr>
                                    <tr><th> Fc Kdjabstruk </th><td> {{ $struktural->fc_kdjabstruk }} </td></tr><tr><th> Fv Nmjabstruk </th><td> {{ $struktural->fv_nmjabstruk }} </td></tr><tr><th> Fc Singkatan </th><td> {{ $struktural->fc_singkatan }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
