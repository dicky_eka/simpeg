@extends('layouts.app', ['activePage' => 'jusuran-pendidikan', 'titlePage' => __('Jusuran Pendidikan')])


@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create New Jusuran Pendidikan</div>
                <div class="card-body">
                    <a href="{{ url('/jusuran-pendidikan') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />

                    @if ($errors->any())
                    <div class="alert alert-danger"  role="alert">
                        <ul >
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br>
                    @endif


                    <form method="POST" action="{{ url('/jusuran-pendidikan') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @include ('jusuran-pendidikan.form', ['formMode' => 'create'])

                    </form>

                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
