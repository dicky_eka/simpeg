@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">JusuranPendidikan {{ $jusuranpendidikan->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/jusuran-pendidikan') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/jusuran-pendidikan/' . $jusuranpendidikan->id . '/edit') }}" title="Edit JusuranPendidikan"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('jusuranpendidikan' . '/' . $jusuranpendidikan->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete JusuranPendidikan" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $jusuranpendidikan->id }}</td>
                                    </tr>
                                    <tr><th> Fc Kdjurpend </th><td> {{ $jusuranpendidikan->fc_kdjurpend }} </td></tr><tr><th> Fv Nmjur </th><td> {{ $jusuranpendidikan->fv_nmjur }} </td></tr><tr><th> Fc Kdpend </th><td> {{ $jusuranpendidikan->fc_kdpend }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
