@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Jabatan {{ $jabatan->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/jabatan') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/jabatan/' . $jabatan->id . '/edit') }}" title="Edit Jabatan"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('jabatan' . '/' . $jabatan->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Jabatan" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $jabatan->id }}</td>
                                    </tr>
                                    <tr><th> Fc Kdjabatan </th><td> {{ $jabatan->fc_kdjabatan }} </td></tr><tr><th> Fv Jabatan </th><td> {{ $jabatan->fv_jabatan }} </td></tr><tr><th> Fc Kdsebut </th><td> {{ $jabatan->fc_kdsebut }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
