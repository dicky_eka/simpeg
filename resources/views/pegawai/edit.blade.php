@extends('layouts.app', ['activePage' => 'pegawai', 'titlePage' => __('Pegawai')])




@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
                    <div class="card-header">Edit Pegawai #{{ $pegawai->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/pegawai') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/pegawai/' . $pegawai->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('pegawai.form', ['formMode' => 'edit'])

                        </form>

                    </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection

@push('js')
<script type="text/javascript">
$(document).ready(function() {
    $('.select2').select2();
    $('#provinsi').change(function(){
        if($(this).val() != ''){
            let id = $(this).val()
            $.get(`/kota/${id}`, function(data){
                var options = ''
                $.each(data,function(key,value)
                {
                    options += '<option value=' + value.fc_kdkota + '>' + value.fv_nmkota + '</option>';
                });
                $('#kota').html(options);
            });
        }
    });
    $('#kota').change(function(){
        if($(this).val() != ''){
            let id = $(this).val()
            $.get(`/kecamatan/${id}`, function(data){
                var options = ''
                $.each(data,function(key,value)
                {
                    options += '<option value=' + value.fc_kdkec + '>' + value.fv_nmKec + '</option>';
                });
                $('#kecamatan').html(options);
            });
        }
    });

    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down",
            previous: "fa fa-chevron-left",
            next: "fa fa-chevron-right",
            today: "fa fa-clock-o",
            clear: "fa fa-trash-o"
        }
    });


});
</script>
@endpush


 