
<div class="row">
    <div class="col-md-9">
        <div class="card">
            <div class="card-body">
                    
                    <div class="form-group {{ $errors->has('fc_nipbr') ? 'has-error' : ''}}">
                        <label for="fc_nip" class="control-label">{{ 'NIP' }}</label>
                         <input class="form-control" name="fc_nipbr" type="text" id="fc_nipbr" value="{{ isset($pegawai->fc_nipbr) ? $pegawai->fc_nipbr : ''}}" >
                        {!! $errors->first('fc_nipbr', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('fc_nip') ? 'has-error' : ''}}">
                        <label for="fc_nip" class="control-label">{{ 'NIP Lama' }}</label>
                         <input class="form-control" name="fc_nip" type="text" id="fc_nip" value="{{ isset($pegawai->FV_TELP) ? $pegawai->fc_nip : ''}}" >
                        {!! $errors->first('fc_nip', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('fc_glrdpn') ? 'has-error' : ''}}">
                            <label for="fc_glrdpn" class="control-label">{{ 'Nama' }}</label>
                             <input class="form-control" name="fc_glrdpn" type="text" id="fc_glrdpn" value="{{ isset($pegawai->fc_glrdpn) ? $pegawai->fc_glrdpn : ''}}" >
                            {!! $errors->first('fc_glrdpn', '<p class="help-block">:message</p>') !!}
                        </div>
                            
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('fv_nama') ? 'has-error' : ''}}">
                            <label for="fv_nama" class="control-label">&nbsp;</label>
                             <input class="form-control" name="fv_nama" type="text" id="fv_nama" value="{{ isset($pegawai->fv_nama) ? $pegawai->fv_nama : ''}}" >
                            {!! $errors->first('fv_nama', '<p class="help-block">:message</p>') !!}
                        </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('fc_glrblk') ? 'has-error' : ''}}">
                                <label for="fc_glrblk" class="control-label">&nbsp;</label>
                                 <input class="form-control" name="fc_glrblk" type="text" id="FV_TELP" value="{{ isset($pegawai->fc_glrblk) ? $pegawai->fc_glrblk : ''}}" >
                                {!! $errors->first('fc_glrblk', '<p class="help-block">:message</p>') !!}
                            </div>
                            
                        </div>
                        
                        
                        
                    </div>

            </div>

        </div>
        <div class="card">
          <div class="card-header card-header-text card-header-primary">
            <div class="card-text">
              <h4 class="card-title">Data Domisili</h4>
            </div>
          </div>
          <div class="card-body">

            <div class="form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                <label for="alamat" class="control-label">{{ 'Alamat' }}</label>
                <input class="form-control" name="alamat" type="text" id="alamat" value="{{ isset($pegawai->fv_alamat) ? $pegawai->fv_alamat : ''}}" >
                {!! $errors->first('fv_alamat', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fc_nip') ? 'has-error' : ''}}">
                        <label for="fc_nip" class="control-label">{{ 'Provinsi' }}</label>
                        <select class="select2 form-control" id="provinsi">
                        @foreach($provinsi as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>

                    </div>
                    <div class="form-group {{ $errors->has('fc_nip') ? 'has-error' : ''}}">
                        <label for="fc_nip" class="control-label">{{ 'Kab/Kota' }}</label>
                        <select class="select2 form-control" id="kota">
                            <option value="">Pilih Kab/Kota</option>
                        </select>
                    </div>
                    <div class="form-group {{ $errors->has('fc_nip') ? 'has-error' : ''}}">
                        <label for="fc_nip" class="control-label">{{ 'Kecamatan' }}</label>
                         <select class="select2 form-control" id="kecamatan">
                            <option value="">Pilih Kecamatan</option>

                        </select>
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('FV_TELP') ? 'has-error' : ''}}">
                        <label for="FV_TELP" class="control-label">{{ 'Telp' }}</label>
                        <input class="form-control" name="fc_nip" type="text" id="FV_TELP" value="{{ isset($pegawai->FV_TELP) ? $pegawai->FV_TELP : ''}}" >
                        {!! $errors->first('FV_TELP', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('FV_HP') ? 'has-error' : ''}}">
                        <label for="FV_HP" class="control-label">{{ 'HP' }}</label>
                        <input class="form-control" name="fc_nip" type="text" id="FV_HP" value="{{ isset($pegawai->FV_HP) ? $pegawai->FV_HP : ''}}" >
                        {!! $errors->first('FV_HP', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('fc_nip') ? 'has-error' : ''}}">
                        <label for="fc_nip" class="control-label">{{ 'Kode POS' }}</label>
                        <input class="form-control" name="fc_nip" type="text" id="fc_nip" value="{{ isset($pegawai->fc_nip) ? $pegawai->fc_nip : ''}}" >
                        {!! $errors->first('fc_nip', '<p class="help-block">:message</p>') !!}
                    </div>
                    
                </div>
                
            </div>
            

            

          </div>
        </div>
        <div class="card">
          <div class="card-header card-header-text card-header-primary">
            <div class="card-text">
              <h4 class="card-title">Data Pribadi dan Pendidikan</h4>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fc_nip') ? 'has-error' : ''}}">
                        <label for="fc_nip" class="control-label">{{ 'Tempat Lahir' }}</label>
                        <select class="select2 form-control">
                        @foreach($kota as $key => $val)
                          <option value="{{$val}}">{{$key}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fd_tgllhr') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Tgl Lahir' }}</label>

                        <input class="form-control datepicker" name="tgl_lahir" type="text" id="alamat" value="{{ isset($pegawai->fd_tgllhr) ? $pegawai->fd_tgllhr : ''}}" >
                        {!! $errors->first('fd_tgllhr', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group {{ $errors->has('fc_kdsex') ? 'has-error' : ''}}">
                        <label for="fc_kdsex" class="control-label">{{ 'Jns. Kel' }}</label>
                        <select class=" form-control" name="fc_kdsex">
                        @foreach($sex as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group {{ $errors->has('fc_Nikah') ? 'has-error' : ''}}">
                        <label for="fc_Nikah" class="control-label">{{ 'Status Nikah' }}</label>
                         <select class="form-control">
                         @foreach($status as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group {{ $errors->has('fd_tglnikah') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Tgl Nikah' }}</label>

                        <input class="form-control datepicker" name="fd_tglnikah" type="text" id="alamat" value="{{ isset($pegawai->fd_tglnikah) ? $pegawai->fd_tglnikah : ''}}" >
                        {!! $errors->first('fd_tglnikah', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-md-3">
                     <div class="form-group {{ $errors->has('fc_golDarah') ? 'has-error' : ''}}">
                        <label for="fc_golDarah" class="control-label">{{ 'Gol. Darah' }}</label>
                         <select class="form-control" name="fc_golDarah">
                         @foreach($darah as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fc_tkpddk') ? 'has-error' : ''}}">
                        <label for="fc_tkpddk" class="control-label">{{ 'Tk.Pendidikan' }}</label>
                        <select class=" form-control" name="fc_tkpddk">
                        @foreach($strataPendidikan as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fc_pdk') ? 'has-error' : ''}}">
                        <label for="fc_tkpddk" class="control-label">{{ 'Jurusan' }}</label>
                         <select class="select2 form-control">
                         @foreach($jurusanPendidikan as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('fc_thnlulus') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Tahun Lulus' }}</label>

                        <input class="form-control datepicker" name="fc_thnlulus" type="text" id="alamat" value="{{ isset($pegawai->fc_thnlulus) ? $pegawai->fc_thnlulus : ''}}" >
                        {!! $errors->first('fc_thnlulus', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-md-4">
                     <div class="form-group {{ $errors->has('fc_penddk') ? 'has-error' : ''}}">
                        <label for="fc_penddk" class="control-label">{{ 'Pendidikan Awal' }}</label>
                         <select class="form-control" name="fc_penddk">
                         @foreach($strataPendidikan as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                     <div class="form-group {{ $errors->has('fc_kdagama') ? 'has-error' : ''}}">
                        <label for="fc_kdagama" class="control-label">{{ 'Agama' }}</label>
                         <select class="form-control" name="fc_kdagama">
                         @foreach($agama as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
            </div>
          </div>

        </div>
        <div class="card">
          <div class="card-header card-header-text card-header-primary">
            <div class="card-text">
              <h4 class="card-title">Data Kepegawaian</h4>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Awal Gol' }}</label>
                         <select class="form-control" name="fc_kdagama">
                             @foreach($golongan as $key => $val)
                              <option value="{{$key}}">{{$val}}</option>
                              @endforeach
                            </select>
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'TMT CPNS' }}</label>
                        <input class="form-control datepicker" name="tgl_lahir" type="text" id="alamat" value="{{ isset($pegawai->fd_tgllhr) ? $pegawai->fd_tgllhr : ''}}" >
                            {!! $errors->first('fd_tgllhr', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'TMT PNS' }}</label>
                        <input class="form-control datepicker" name="tgl_lahir" type="text" id="alamat" value="{{ isset($pegawai->fd_tgllhr) ? $pegawai->fd_tgllhr : ''}}" >
                            {!! $errors->first('fd_tgllhr', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Status Kepeg' }}</label>
                         <select class="form-control" name="fc_kdagama">
                             @foreach($statusPegawai as $key => $val)
                              <option value="{{$key}}">{{$val}}</option>
                              @endforeach
                            </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Jenis Peg' }}</label>
                         <select class="form-control" name="fc_kdagama">
                             @foreach($jenisPegawai as $key => $val)
                              <option value="{{$key}}">{{$val}}</option>
                              @endforeach
                            </select>
                    </div>
                </div>
               

            </div>
            <div class="row">
                 <div class="col-md-3">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Gol/Ruang' }}</label>
                        <select class="form-control" name="fc_kdagama">
                             @foreach($golongan as $key => $val)
                              <option value="{{$key}}">{{$val}}</option>
                              @endforeach
                            </select>
                    </div>
                </div>
                 <div class="col-md-3">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'TMT GOl' }}</label>
                        <input class="form-control datepicker" name="tgl_lahir" type="text" id="alamat" value="{{ isset($pegawai->fd_tgllhr) ? $pegawai->fd_tgllhr : ''}}" >
                            {!! $errors->first('fd_tgllhr', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                 <div class="col-md-3">
                    <div class=" form-group {{ $errors->has('fc_ThnKerja') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Masa Kerja (Tahun)' }}</label>
                        <input class="form-control" name="fc_ThnKerja" type="text" id="fc_ThnKerja" value="{{ isset($pegawai->fv_alamat) ? $pegawai->fc_ThnKerja : ''}}" >
                        {!! $errors->first('fv_alamat', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class=" form-group {{ $errors->has('fc_BlnKerja') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Masa Kerja (Bulan)' }}</label>
                        <input class="form-control" name="alamat" type="text" id="alamat" value="{{ isset($pegawai->fc_BlnKerja) ? $pegawai->fc_BlnKerja : ''}}" >
                        {!! $errors->first('fc_BlnKerja', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                 <div class="col-md-3">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Eselon' }}</label>
                        <select class="form-control" name="fc_kdagama">
                         @foreach($eselon as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
                 <div class="col-md-3">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'TMT Eselon' }}</label>
                        <input class="form-control datepicker" name="tgl_lahir" type="text" id="alamat" value="{{ isset($pegawai->fd_tgllhr) ? $pegawai->fd_tgllhr : ''}}" >
                        {!! $errors->first('fd_tgllhr', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                 <div class="col-md-3">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Eselon Tertinggi' }}</label>
                        <select class="form-control" name="fc_kdagama">
                         @foreach($eselon as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'TMT Eselon Tertinggi' }}</label>
                         <input class="form-control datepicker" name="tgl_lahir" type="text" id="alamat" value="{{ isset($pegawai->fd_tgllhr) ? $pegawai->fd_tgllhr : ''}}" >
                        {!! $errors->first('fd_tgllhr', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                 <div class="col-md-4">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Pensiun/Mutasi/Meninggal' }}</label>
                        <select class="form-control" name="fc_kdagama">
                         @foreach($pensiun as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'TMT Pensiun' }}</label>
                        <input class="form-control datepicker" name="tgl_lahir" type="text" id="alamat" value="{{ isset($pegawai->fd_tgllhr) ? $pegawai->fd_tgllhr : ''}}" >
                        {!! $errors->first('fd_tgllhr', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Mutasi Masuk,TMT' }}</label>
                       <input class="form-control datepicker" name="tgl_lahir" type="text" id="alamat" value="{{ isset($pegawai->fd_tgllhr) ? $pegawai->fd_tgllhr : ''}}" >
                        {!! $errors->first('fd_tgllhr', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
             
            <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                <label for="alamat" class="control-label">{{ 'Unit Kerja' }}</label>
                <input class="form-control" name="alamat" type="text" id="alamat" value="" >
                {!! $errors->first('fv_alamat', '<p class="help-block">:message</p>') !!}
            </div>
            <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                <label for="alamat" class="control-label">{{ 'SatKer' }}</label>
                <input class="form-control" name="alamat" type="text" id="alamat" value="" >
                {!! $errors->first('fv_alamat', '<p class="help-block">:message</p>') !!}
            </div>
            <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                <label for="alamat" class="control-label">{{ 'Sub SatKer' }}</label>
                <input class="form-control" name="alamat" type="text" id="alamat" value="" >
                {!! $errors->first('fv_alamat', '<p class="help-block">:message</p>') !!}
            </div>
            <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                <label for="alamat" class="control-label">{{ 'Seksi' }}</label>
                <input class="form-control" name="alamat" type="text" id="alamat" value="" >
                {!! $errors->first('fv_alamat', '<p class="help-block">:message</p>') !!}
            </div>
            <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                <label for="alamat" class="control-label">{{ 'Diperbantu Ke' }}</label>
                <input class="form-control" name="alamat" type="text" id="alamat" value="" >
                {!! $errors->first('fv_alamat', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="row">
                 <div class="col-md-6">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Jenis Jabatan' }}</label>
                        <select class="form-control" name="fc_kdagama">
                         @foreach($jenisJabatan as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'JFU/JFT' }}</label>
                        <input class="form-control" name="alamat" type="text" id="alamat" value="" >
                        {!! $errors->first('fv_alamat', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

            </div>
            <div class="row">
                 <div class="col-md-6">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Jab. Struktural' }}</label>
                         <select class="form-control" name="fc_kdagama">
                         @foreach($jabatanStruktural as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'TMT Jab. Struktural' }}</label>
                        <input class="form-control datepicker" name="tgl_lahir" type="text" id="alamat" value="" >
                    </div>
                </div>

            </div>
            <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                <label for="alamat" class="control-label">{{ 'Jab. Fungsional' }}</label>
                <select class="form-control" name="fc_kdagama">
                 @foreach($jabatanFungsional as $key => $val)
                  <option value="{{$key}}">{{$val}}</option>
                  @endforeach
                </select>
            </div>
             <div class="row">
                 
                 <div class="col-md-6">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'TMT Jab. Fungsional' }}</label>
                        <input class="form-control datepicker" name="tgl_lahir" type="text" id="alamat" value="" >
                    </div>
                </div>
                <div class="col-md-6">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Angka Kredit' }}</label>
                        <input class="form-control" name="alamat" type="text" id="alamat" value="" >
                        {!! $errors->first('fv_alamat', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

            </div>
            <div class="row">
                 <div class="col-md-6">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Diktlat Struktural Terkahir' }}</label>
                        <select class="form-control" name="fc_kdagama">
                         @foreach($diklatStruktural as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'TMT Diktlat Struktural' }}</label>
                        <input class="form-control datepicker" name="fc_thnlulus" type="text" id="alamat" value="" >
                    </div>
                </div>

            </div>
            <div class="row">
                 <div class="col-md-6">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Diktlat Fungsional Terkahir' }}</label>
                         <select class="form-control" name="fc_kdagama">
                         @foreach($diklatFungsional as $key => $val)
                          <option value="{{$key}}">{{$val}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
                

            </div>
            <div class="row">
                 <div class="col-md-6">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'TMT KGB' }}</label>
                         <input class="form-control datepicker" name="fc_thnlulus" type="text" id="alamat" value="" >
                    </div>
                </div>
                <div class="col-md-6">
                    <div class=" form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Masa Kerja KGB (Tahun)' }}</label>
                        <input class="form-control" name="alamat" type="text" id="alamat" value="" >
                        {!! $errors->first('fv_alamat', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

            </div>




            
          </div>
          
        </div>
        <div class="card">
          <div class="card-header card-header-text card-header-primary">
            <div class="card-text">
              <h4 class="card-title">Data Pendukung</h4>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'Bapertum' }}</label>
                        <input class="form-control" name="alamat" type="text" id="alamat" value="" >
                        {!! $errors->first('fv_alamat', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('fc_nip') ? 'has-error' : ''}}">
                        <label for="fc_nip" class="control-label">{{ 'NO. TASPEN' }}</label>
                        <input class="form-control" name="fc_nip" type="text" id="fc_nip" value="" >
                        {!! $errors->first('fc_nip', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('fc_nip') ? 'has-error' : ''}}">
                        <label for="fc_nip" class="control-label">{{ 'Nomor ASKES' }}</label>
                        <input class="form-control" name="fc_nip" type="text" id="fc_nip" value="" >
                        {!! $errors->first('fc_nip', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('fc_nip') ? 'has-error' : ''}}">
                        <label for="fc_nip" class="control-label">{{ 'Asuransi' }}</label>
                        <input class="form-control" name="fc_nip" type="text" id="fc_nip" value="" >
                        {!! $errors->first('fc_nip', '<p class="help-block">:message</p>') !!}
                    </div>                    
                     <div class="form-group {{ $errors->has('fc_npwp') ? 'has-error' : ''}}">
                        <label for="fc_npwp" class="control-label">{{ 'NPWP' }}</label>
                        <input class="form-control" name="fc_npwp" type="text" id="fc_npwp" value="{{ isset($pegawai->fc_npwp) ? $pegawai->fc_npwp : ''}}" >
                        {!! $errors->first('fc_npwp', '<p class="help-block">:message</p>') !!}
                    </div>       
                     <div class="form-group {{ $errors->has('fc_nik') ? 'has-error' : ''}}">
                        <label for="fc_nip" class="control-label">{{ 'NIK' }}</label>
                        <input class="form-control" name="fc_nik" type="text" id="fc_nik"  value="{{ isset($pegawai->fc_nik) ? $pegawai->fc_nik : ''}}" >
                        {!! $errors->first('fc_nik', '<p class="help-block">:message</p>') !!}
                    </div>       
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'KARPEG' }}</label>
                        <input class="form-control" name="alamat" type="text" id="alamat" value="" >
                        {!! $errors->first('fv_alamat', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('fv_alamat') ? 'has-error' : ''}}">
                        <label for="alamat" class="control-label">{{ 'NO BPJS' }}</label>
                        <input class="form-control" name="alamat" type="text" id="alamat" value="" >
                        {!! $errors->first('fv_alamat', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('FV_TELP') ? 'has-error' : ''}}">
                        <label for="FV_TELP" class="control-label">{{ 'NO ARSIP' }}</label>
                        <input class="form-control" name="fc_nip" type="text" id="FV_TELP" value="" >
                        {!! $errors->first('FV_TELP', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('FV_TELP') ? 'has-error' : ''}}">
                        <label for="FV_TELP" class="control-label">{{ 'Kode Induk Inst' }}</label>
                        <input class="form-control" name="fc_nip" type="text" id="FV_TELP" value="" >
                        {!! $errors->first('FV_TELP', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('FV_TELP') ? 'has-error' : ''}}">
                        <label for="FV_TELP" class="control-label">{{ 'KARIS/KARSI' }}</label>
                        <input class="form-control" name="fc_nip" type="text" id="FV_TELP" value="" >
                        {!! $errors->first('FV_TELP', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('fv_email') ? 'has-error' : ''}}">
                        <label for="fv_email" class="control-label">{{ 'EMAIL' }}</label>
                        <input class="form-control" name="fv_email" type="text" id="fv_email" value="{{ isset($pegawai->fv_email) ? $pegawai->fv_email : ''}}" >
                        {!! $errors->first('FV_TELP', '<p class="help-block">:message</p>') !!}
                    </div>
                     <div class="form-group {{ $errors->has('FV_TELP') ? 'has-error' : ''}}">
                        <label for="FV_TELP" class="control-label">{{ 'No Rek' }}</label>
                        <input class="form-control" name="fc_nip" type="text" id="FV_TELP" value="{{ isset($pegawai->FV_TELP) ? $pegawai->FV_TELP : ''}}" >
                        {!! $errors->first('FV_TELP', '<p class="help-block">:message</p>') !!}
                    </div>
                    
                    
                </div>
                
            </div>
          </div>

        </div>

 
    </div>
    <div class="col-md-3">
        <div class="card">
          <div class="card-header card-header-text card-header-primary">
            <div class="card-text">
              <h4 class="card-title">Foto Pegawai</h4>
            </div>
          </div>
          <div class="card-body">
            <img src="http://style.anu.edu.au/_anu/4/images/placeholders/person_8x10.png" class="img-thumbnail">
            <br>
            <br>
            <div>
                <input type="file" class="form-control-file" >
            </div>
        </div>
 
          </div>
      </div>
        
    </div>
</div>
<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
