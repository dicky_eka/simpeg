@extends('layouts.app', ['activePage' => 'jabatan-fungsional', 'titlePage' => __('Jabatan Fungsional')])

@section('content')
  <div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Jabatan Fungsional </div>
                    <div class="card-body">
                        <a href="{{ url('/jabatan-fungsional/create') }}" class="btn btn-success btn-sm" title="Add New JabatanFungsional">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <form method="GET" action="{{ url('/jabatan-fungsional') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                            <button class="btn btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        
                                        <th>Fc Kdjabfung</th>
                                        <th>Fv Nmjabfung</th>
                                        <th>Fc Kdtenaga</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($jabatanfungsional as $item)
                                    <tr>
                                        <td>{{ $item->fc_kdjabfung }}</td>
                                        <td>{{ $item->fv_nmjabfung }}</td>
                                        <td>{{ $item->fc_kdtenaga }}</td>
                                        <td>
                                            <a href="{{ url('/jabatan-fungsional/' . $item->fc_kdjabfung . '/edit') }}" title="Edit JabatanFungsional">
                                                <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
                                            </a>

                                            <form method="POST" action="{{ url('/jabatan-fungsional' . '/' . $item->fc_kdjabfung) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }} {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete JabatanFungsional" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $jabatanfungsional->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
