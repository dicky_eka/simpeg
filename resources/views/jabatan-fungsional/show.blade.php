@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">JabatanFungsional {{ $jabatanfungsional->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/jabatan-fungsional') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/jabatan-fungsional/' . $jabatanfungsional->id . '/edit') }}" title="Edit JabatanFungsional"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('jabatanfungsional' . '/' . $jabatanfungsional->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete JabatanFungsional" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $jabatanfungsional->id }}</td>
                                    </tr>
                                    <tr><th> Fc Kdjabfung </th><td> {{ $jabatanfungsional->fc_kdjabfung }} </td></tr><tr><th> Fv Nmjabfung </th><td> {{ $jabatanfungsional->fv_nmjabfung }} </td></tr><tr><th> Fc Kdtenaga </th><td> {{ $jabatanfungsional->fc_kdtenaga }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
