@extends('layouts.app', ['activePage' => 'jabatan-fungsional', 'titlePage' => __('Jabatan Fungsional')])


@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit Jabatan Fungsional #{{ $jabatanfungsional->fc_kdjabfung }}</div>
                <div class="card-body">
                    <a href="{{ url('/jabatan-fungsional') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />

                    @if ($errors->any())
                    <div class="alert alert-danger"  role="alert">
                        <ul >
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br>
                    <br>
                    @endif

                    <form action="{{ route('jabatan-fungsional.update',$jabatanfungsional->fc_kdjabfung) }}" method="POST">
                        @csrf
                        @method('PUT')

                        @include ('jabatan-fungsional.form', ['formMode' => 'edit'])

                    </form>

                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
