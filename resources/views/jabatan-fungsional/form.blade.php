<div class="form-group {{ $errors->has('fc_kdjabfung') ? 'has-error' : ''}}">
    <label for="fc_kdjabfung" class="control-label">{{ 'Fc Kdjabfung' }}</label>
    <input class="form-control" name="fc_kdjabfung" type="text" id="fc_kdjabfung" value="{{ isset($jabatanfungsional->fc_kdjabfung) ? $jabatanfungsional->fc_kdjabfung : ''}}" >
    {!! $errors->first('fc_kdjabfung', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fv_nmjabfung') ? 'has-error' : ''}}">
    <label for="fv_nmjabfung" class="control-label">{{ 'Fv Nmjabfung' }}</label>
    <input class="form-control" name="fv_nmjabfung" type="text" id="fv_nmjabfung" value="{{ isset($jabatanfungsional->fv_nmjabfung) ? $jabatanfungsional->fv_nmjabfung : ''}}" >
    {!! $errors->first('fv_nmjabfung', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fc_kdtenaga') ? 'has-error' : ''}}">
    <label for="fc_kdtenaga" class="control-label">{{ 'Fc Kdtenaga' }}</label>
    <input class="form-control" name="fc_kdtenaga" type="text" id="fc_kdtenaga" value="{{ isset($jabatanfungsional->fc_kdtenaga) ? $jabatanfungsional->fc_kdtenaga : ''}}" >
    {!! $errors->first('fc_kdtenaga', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fc_kdgolmax') ? 'has-error' : ''}}">
    <label for="fc_kdgolmax" class="control-label">{{ 'Fc Kdgolmax' }}</label>
    <input class="form-control" name="fc_kdgolmax" type="text" id="fc_kdgolmax" value="{{ isset($jabatanfungsional->fc_kdgolmax) ? $jabatanfungsional->fc_kdgolmax : ''}}" >
    {!! $errors->first('fc_kdgolmax', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fc_kdgolmin') ? 'has-error' : ''}}">
    <label for="fc_kdgolmin" class="control-label">{{ 'Fc Kdgolmin' }}</label>
    <input class="form-control" name="fc_kdgolmin" type="text" id="fc_kdgolmin" value="{{ isset($jabatanfungsional->fc_kdgolmin) ? $jabatanfungsional->fc_kdgolmin : ''}}" >
    {!! $errors->first('fc_kdgolmin', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fm_tunjangan') ? 'has-error' : ''}}">
    <label for="fm_tunjangan" class="control-label">{{ 'Fm Tunjangan' }}</label>
    <input class="form-control" name="fm_tunjangan" type="number" id="fm_tunjangan" value="{{ isset($jabatanfungsional->fm_tunjangan) ? $jabatanfungsional->fm_tunjangan : ''}}" >
    {!! $errors->first('fm_tunjangan', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fn_bup') ? 'has-error' : ''}}">
    <label for="fn_bup" class="control-label">{{ 'Fn Bup' }}</label>
    <input class="form-control" name="fn_bup" type="number" id="fn_bup" value="{{ isset($jabatanfungsional->fn_bup) ? $jabatanfungsional->fn_bup : ''}}" >
    {!! $errors->first('fn_bup', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
