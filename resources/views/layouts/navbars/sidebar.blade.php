<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="#" class="simple-text logo-normal">
      {{ __('SIMPEG') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'user-management') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#laravelExample" aria-expanded="true">
         <i class="material-icons">table_chart</i>
          <p>{{ __('Data Master') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse show" id="laravelExample">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'golongan' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('golongan.index') }}">
                <span class="sidebar-normal">{{ __('Golongan/Ruang') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'eselon' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('eselon.index') }}">
                <span class="sidebar-normal"> {{ __('Eselon') }} </span>
              </a>
            </li>
             <li class="nav-item{{ $activePage == 'strata-pendidikan' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('strata-pendidikan.index') }}">
                <span class="sidebar-normal"> {{ __('Strata Pendidikan') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'jusuran-pendidikan' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('jusuran-pendidikan.index') }}">
                <span class="sidebar-normal"> {{ __('Jurusan Pendidikan') }} </span>
              </a>
            </li>

             <li class="nav-item{{ $activePage == 'jabatan' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('jabatan.index') }}">
                <span class="sidebar-normal"> {{ __('Rumpun Jabatan CPNS') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'jabatan-struktural' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('jabatan-struktural.index') }}">
                <span class="sidebar-normal"> {{ __('Jabatan Struktural') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'jabatan-fungsional' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('jabatan-fungsional.index') }}">
                <span class="sidebar-normal"> {{ __('Jabatan Fungsional') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'gaji-pokok' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('gaji-pokok.index') }}">
                <span class="sidebar-normal"> {{ __('Gaji Pokok') }} </span>
              </a>
            </li>
             

          </ul>
        </div>
      </li>
      <li class="nav-item{{ $activePage == 'pegawai' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('pegawai.index') }}">
          <i class="material-icons">supervisor_account</i>
            <p>{{ __('Pegawai') }}</p>
        </a>
      </li>
    </ul>
  </div>
</div>