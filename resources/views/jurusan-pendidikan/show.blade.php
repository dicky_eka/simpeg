@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">JurusanPendidikan {{ $jurusanpendidikan->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/jurusan-pendidikan') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/jurusan-pendidikan/' . $jurusanpendidikan->id . '/edit') }}" title="Edit JurusanPendidikan"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('jurusanpendidikan' . '/' . $jurusanpendidikan->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete JurusanPendidikan" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $jurusanpendidikan->id }}</td>
                                    </tr>
                                    <tr><th> Fc Kdjurpend </th><td> {{ $jurusanpendidikan->fc_kdjurpend }} </td></tr><tr><th> Fv Nmjur </th><td> {{ $jurusanpendidikan->fv_nmjur }} </td></tr><tr><th> Fc Kdpend </th><td> {{ $jurusanpendidikan->fc_kdpend }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
