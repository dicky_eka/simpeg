<div class="form-group {{ $errors->has('fc_kdjurpend') ? 'has-error' : ''}}">
    <label for="fc_kdjurpend" class="control-label">{{ 'Fc Kdjurpend' }}</label>
    <input class="form-control" name="fc_kdjurpend" type="text" id="fc_kdjurpend" value="{{ isset($jurusanpendidikan->fc_kdjurpend) ? $jurusanpendidikan->fc_kdjurpend : ''}}" >
    {!! $errors->first('fc_kdjurpend', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fv_nmjur') ? 'has-error' : ''}}">
    <label for="fv_nmjur" class="control-label">{{ 'Fv Nmjur' }}</label>
    <input class="form-control" name="fv_nmjur" type="text" id="fv_nmjur" value="{{ isset($jurusanpendidikan->fv_nmjur) ? $jurusanpendidikan->fv_nmjur : ''}}" >
    {!! $errors->first('fv_nmjur', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fc_kdpend') ? 'has-error' : ''}}">
    <label for="fc_kdpend" class="control-label">{{ 'Fc Kdpend' }}</label>
    <input class="form-control" name="fc_kdpend" type="text" id="fc_kdpend" value="{{ isset($jurusanpendidikan->fc_kdpend) ? $jurusanpendidikan->fc_kdpend : ''}}" >
    {!! $errors->first('fc_kdpend', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fc_kdgroupjur') ? 'has-error' : ''}}">
    <label for="fc_kdgroupjur" class="control-label">{{ 'Fc Kdgroupjur' }}</label>
    <input class="form-control" name="fc_kdgroupjur" type="text" id="fc_kdgroupjur" value="{{ isset($jurusanpendidikan->fc_kdgroupjur) ? $jurusanpendidikan->fc_kdgroupjur : ''}}" >
    {!! $errors->first('fc_kdgroupjur', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
