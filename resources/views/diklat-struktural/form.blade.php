<div class="form-group {{ $errors->has('fc_kdDiklat') ? 'has-error' : ''}}">
    <label for="fc_kdDiklat" class="control-label">{{ 'Fc Kddiklat' }}</label>
    <input class="form-control" name="fc_kdDiklat" type="text" id="fc_kdDiklat" value="{{ isset($diklatstruktural->fc_kdDiklat) ? $diklatstruktural->fc_kdDiklat : ''}}" >
    {!! $errors->first('fc_kdDiklat', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fv_nmDiklat') ? 'has-error' : ''}}">
    <label for="fv_nmDiklat" class="control-label">{{ 'Fv Nmdiklat' }}</label>
    <input class="form-control" name="fv_nmDiklat" type="text" id="fv_nmDiklat" value="{{ isset($diklatstruktural->fv_nmDiklat) ? $diklatstruktural->fv_nmDiklat : ''}}" >
    {!! $errors->first('fv_nmDiklat', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
