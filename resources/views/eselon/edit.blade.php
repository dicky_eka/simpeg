@extends('layouts.app', ['activePage' => 'eselon', 'titlePage' => __('Eselon')])


@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit Eselon #{{ $eselon->fc_kdeselon }}</div>
                <div class="card-body">
                    <a href="{{ url('/eselon') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />

                    @if ($errors->any())
                    <div class="alert alert-danger"  role="alert">
                        <ul >
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br />
                    <br />
                        
                    @endif

                    <form action="{{ route('eselon.update',$eselon->fc_kdeselon) }}" method="POST">
                            @csrf
                            @method('PUT')

                        @include ('eselon.form', ['formMode' => 'edit'])

                    </form>

                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
