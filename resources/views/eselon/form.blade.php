<div class="form-group {{ $errors->has('fc_kdeselon') ? 'has-error' : ''}}">
    <label for="fc_kdeselon" class="control-label">{{ 'Fc Kdeselon' }}</label>
    <input class="form-control" name="fc_kdeselon" type="text" id="fc_kdeselon" value="{{ isset($eselon->fc_kdeselon) ? $eselon->fc_kdeselon : ''}}" >
    {!! $errors->first('fc_kdeselon', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fc_eselon') ? 'has-error' : ''}}">
    <label for="fc_eselon" class="control-label">{{ 'Fc Eselon' }}</label>
    <input class="form-control" name="fc_eselon" type="text" id="fc_eselon" value="{{ isset($eselon->fc_eselon) ? $eselon->fc_eselon : ''}}" >
    {!! $errors->first('fc_eselon', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fm_tjStruk') ? 'has-error' : ''}}">
    <label for="fm_tjStruk" class="control-label">{{ 'Fm Tjstruk' }}</label>
    <input class="form-control" name="fm_tjStruk" type="number" id="fm_tjStruk" value="{{ isset($eselon->fm_tjStruk) ? $eselon->fm_tjStruk : ''}}" >
    {!! $errors->first('fm_tjStruk', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fn_usiapensiun') ? 'has-error' : ''}}">
    <label for="fn_usiapensiun" class="control-label">{{ 'Fn Usiapensiun' }}</label>
    <input class="form-control" name="fn_usiapensiun" type="number" id="fn_usiapensiun" value="{{ isset($eselon->fn_usiapensiun) ? $eselon->fn_usiapensiun : ''}}" >
    {!! $errors->first('fn_usiapensiun', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
