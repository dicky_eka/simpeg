<div class="form-group {{ $errors->has('fc_kdjabatan') ? 'has-error' : ''}}">
    <label for="fc_kdjabatan" class="control-label">{{ 'Fc Kdjabatan' }}</label>
    <input class="form-control" name="fc_kdjabatan" type="text" id="fc_kdjabatan" value="{{ isset($post->fc_kdjabatan) ? $post->fc_kdjabatan : ''}}" >
    {!! $errors->first('fc_kdjabatan', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fv_jabatan') ? 'has-error' : ''}}">
    <label for="fv_jabatan" class="control-label">{{ 'Fv Jabatan' }}</label>
    <input class="form-control" name="fv_jabatan" type="text" id="fv_jabatan" value="{{ isset($post->fv_jabatan) ? $post->fv_jabatan : ''}}" >
    {!! $errors->first('fv_jabatan', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fc_kdsebut') ? 'has-error' : ''}}">
    <label for="fc_kdsebut" class="control-label">{{ 'Fc Kdsebut' }}</label>
    <input class="form-control" name="fc_kdsebut" type="text" id="fc_kdsebut" value="{{ isset($post->fc_kdsebut) ? $post->fc_kdsebut : ''}}" >
    {!! $errors->first('fc_kdsebut', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
