<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware' => 'auth'], function () {
	Route::get('/', 'HomeController@index')->name('home');
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

	Route::resource('jabatan', 'JabatanController');
	Route::resource('golongan', 'GolonganController');
	Route::resource('eselon', 'EselonController');
	Route::resource('jabatan-struktural', 'JabatanStrukturalController');
	Route::resource('jabatan-fungsional', 'JabatanFungsionalController');
	Route::resource('gaji-pokok', 'GajiPokokController');

	Route::resource('jusuran-pendidikan', 'JusuranPendidikanController');
	Route::resource('strata-pendidikan', 'StrataPendidikanController');
	Route::resource('diklat-profesi', 'DiklatProfesiController');
	Route::resource('diklat-fungsional', 'DiklatFungsionalController');
	Route::resource('diklat-fungsional', 'DiklatFungsionalController');
	Route::resource('diklat-struktural', 'DiklatStrukturalController');
	Route::resource('pegawai', 'PegawaiController');


	Route::get('kota/{provinsiID}', 'WilayahController@getKota');
	Route::get('kecamatan/{kotaID}', 'WilayahController@getKecamatan');




});



